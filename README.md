# Minibookshop

https://github.com/40ants/weblocks/blob/reblocks/docs/source/quickstart.rst

https://gitlab.com/vindarel/cl-bookshops

! In development, don't look ! (but it's awesome)

## Usage

    (ql:quickload "minibookshop")

    (start)


## Installation

Needs weblocks (reblocks), weblocks-ui, weblocks-parenscript & our cl-bookshops in QL's local projects.

https://github.com/40ants/weblocks-ui
https://github.com/40ants/weblocks-parenscript
https://github.com/vindarel/cl-bookshops

## Develop

    (weblocks/debug:reset-latest-session)

> can not find action: 022tdefe…

reload the page.
